# Exercise 10 Document Stores

Get a running instance of MongoDB via

```
docker run --name my_mongoDB -d mongo
```

and open a shell inside the container to work on your task.


`wget` to download the dataset inside the container is missing. To get it, run `apt update && apt install wget`.



Finally, after completing the tasks, stop the database again via:

```
docker stop my_mongoDB && docker rm my_mongoDB
```