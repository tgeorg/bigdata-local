# Status: Incomplete ❌

***

# Exercise 08 Spark



Clone [this repo](https://github.com/big-data-europe/docker-spark/)


Change into the cloned directory and run the containers via

```
docker-compose up
```

Copying the notebook into the `spark-master` container and running it however results in an error message about missing `IPyKernel`. Installing via pip however fails on the pip stage.

Which is where I am currently stuck now. If you have an idea on how to fix this, [let me know](https://gitlab.ethz.ch/tgeorg/bigdata-local/-/issues/new).

&nbsp;

Finally to close the containers, `Ctrl-C` in the terminal window used to run the containers or run in the same directory as the original docker compose file.

```
docker-compose down
```
