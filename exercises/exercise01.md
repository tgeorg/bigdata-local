# Exercise 01 SQL Brush Up


Start the postgres database in a docker container:

```
docker run --name my-postgres --publish=5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres
```

Note that the default user's name is `postgres` and the server's hostname is `localhost`.

If installing the `psycopg2` package via pip fails, you probably want to use `psycopg2-binary` instead.

Make sure to remove `?sslmode=require` from

```
connection_string = f'postgresql://{user}:{password}@{server}:5432/{database}?sslmode=require'
```

when running locally (unless you got a trusted certificate for "localhost" ;P)

&nbsp;

Finally when done with the exercise, stop the container again with:

```
docker stop my-postgres && docker rm my-postgres
```
