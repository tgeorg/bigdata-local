# Exercise 05 Hbase



Clone [this repo](https://github.com/big-data-europe/docker-hbase)


Change into the cloned directory and run the containers via

```
docker-compose -f docker-compose-distributed-local.yml up -d
```

and open a shell on the hbase-master to start working on the exercises.

Note that working with Apache Hive inside the containers is not supported due to missing packages.

Furthermore working on the Wikipedia dataset does also not work as the import fails on not knowing the `wasbs://` protocol. If you know another way to access the data, please [let me know](https://gitlab.ethz.ch/tgeorg/bigdata-local/-/issues/new).

&nbsp;

Finally, after completing the exercises, close the containers again with:

```
docker-compose -f docker-compose-distributed-local.yml down
```