# Exercise 03 HDFS

Note that setting up and running Hadoop is not really part the focus of this particular exercise and is meant for experimenting. Regardless here is how you can do with Docker.

Clone [this repo](https://github.com/big-data-europe/docker-hadoop)


Change into the cloned directory and run the containers via

```
docker-compose up
```

You can now access the webinterface like described in the exercise via [http://localhost:9870/](http://localhost:9870/).

&nbsp;

Finally to close the containers, `Ctrl-C` in the terminal window used to run the containers or run in the same directory as the original docker compose file.

```
docker-compose down
```
