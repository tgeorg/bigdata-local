# Status: Incomplete ❌

***

# Exercise 07 MapReduce



Clone [this repo](https://github.com/big-data-europe/docker-hadoop)


Change into the cloned directory and run the containers via

```
docker-compose up
```

and open a shell on the NameNode.

If you are missing a package on the NameNode:

- `wget` install it via `apt install wget` inside the container
- `unzip` install it via `apt install unzip` inside the container
- `mvn` install it via `apt install maven` inside the container

If you get an error running the MapReduce job

```
root@c963017595ed:~/mapreduce# yarn jar ./target/wordcount-1.0-SNAPSHOT.jar MapReduceWordCount /tmp/gutenberg.txt /tmp/results
Exception in thread "main" java.lang.IllegalAccessError: class org.apache.hadoop.hdfs.web.HftpFileSystem cannot access its superinterface org.apache.hadoop.hdfs.web.TokenAspect$TokenManagementDelegator
        at java.lang.ClassLoader.defineClass1(Native Method)
        at java.lang.ClassLoader.defineClass(ClassLoader.java:756)
        at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:142)
        at java.net.URLClassLoader.defineClass(URLClassLoader.java:468)
        at java.net.URLClassLoader.access$100(URLClassLoader.java:74)
        at java.net.URLClassLoader$1.run(URLClassLoader.java:369)
        at java.net.URLClassLoader$1.run(URLClassLoader.java:363)
        at java.security.AccessController.doPrivileged(Native Method)
        at java.net.URLClassLoader.findClass(URLClassLoader.java:362)
        at java.lang.ClassLoader.loadClass(ClassLoader.java:418)
        at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
        at java.lang.Class.forName0(Native Method)
        at java.lang.Class.forName(Class.java:348)
        at java.util.ServiceLoader$LazyIterator.nextService(ServiceLoader.java:370)
        at java.util.ServiceLoader$LazyIterator.next(ServiceLoader.java:404)
        at java.util.ServiceLoader$1.next(ServiceLoader.java:480)
        at org.apache.hadoop.fs.FileSystem.loadFileSystems(FileSystem.java:3217)
        at org.apache.hadoop.fs.FileSystem.getFileSystemClass(FileSystem.java:3262)
        at org.apache.hadoop.fs.FileSystem.createFileSystem(FileSystem.java:3301)
        at org.apache.hadoop.fs.FileSystem.access$200(FileSystem.java:124)
        at org.apache.hadoop.fs.FileSystem$Cache.getInternal(FileSystem.java:3352)
        at org.apache.hadoop.fs.FileSystem$Cache.get(FileSystem.java:3320)
        at org.apache.hadoop.fs.FileSystem.get(FileSystem.java:479)
        at org.apache.hadoop.fs.FileSystem.get(FileSystem.java:227)
        at org.apache.hadoop.fs.FileSystem.get(FileSystem.java:463)
        at org.apache.hadoop.fs.Path.getFileSystem(Path.java:365)
        at org.apache.hadoop.mapreduce.lib.input.FileInputFormat.addInputPath(FileInputFormat.java:542)
        at MapReduceWordCount.main(MapReduceWordCount.java:67)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)
        at org.apache.hadoop.util.RunJar.run(RunJar.java:323)
        at org.apache.hadoop.util.RunJar.main(RunJar.java:236)
root@c963017595ed:~/mapreduce#
```

then you're stuck at the same place as I am.

The internet seems to suggest this is due to different versions of hadoop being used. `pom.xml` specifies version `2.7.3` while the Docker setup uses `3.2.1`. But replacing all Hadoop dependencies with version `3.2.1`, recompiling with `mvn clean package`, and re-running yarn still gives the same error. If you have an idea on how to fix this, [let me know](https://gitlab.ethz.ch/tgeorg/bigdata-local/-/issues/new).

&nbsp;

Finally to close the containers, `Ctrl-C` in the terminal window used to run the containers or run in the same directory as the original docker compose file.

```
docker-compose down
```