# Exercise 12 Graph Databases

We will use the official docker images for neo4j. Start the database via:

```
docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    --name my_neo4j \
    neo4j
```

Note that `--volume=$HOME/neo4j/data:/data` will create folders `neo4j/data/` in your home directory. You can remove it again after finishing the exercises.

&nbsp;

Finally to close the database, `Ctrl-C` in the terminal window used to run the containers or run:

```
docker stop my_neo4j && docker rm my_neo4j
```
