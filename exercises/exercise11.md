# Status: WIP ⭕

***

# Exercise 11 Rumble

There is public instance of rumbledb available via `public.rumbledb.org`. To use it set

```python
server='http://public.rumbledb.org:9090/jsoniq'
```

as described in the notebook. Note that this public instance is shared with other students. Resource intensive queries can overload it and closer to the exam it might see more traffic. Try to be mindful of other students.

&nbsp;

Instructions on setting up Rumble locally might follow.
