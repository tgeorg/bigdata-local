# BigData-local

Instructions on how to solve various exercises from the [BigData lecture](http://vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=140750&semkez=2020W&ansicht=KATALOGDATEN)  locally (i.e. without Azure services).

To prevent issues with local dependencies, bloating your install etc, we will be relying mostly on [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/). Knowledge on how to install and work with these tools is assumed.

Note that all notebooks from the lecture are collected in [this repo](https://github.com/RumbleDB/bigdata-exercises/).

Instructions:

- [Exercise 01 SQL Brush Up](exercises/exercise01.md)
- [Exercise 03 HDFS](exercises/exercise03.md)
- [Exercise 05 HBase](exercises/exercise05.md)
- [Exercise 10 Document Stores](exercises/exercise10.md)
- [Exercise 11 Rumble](exercises/exercise11.md)
- [Exercise 12 Graph Databases](exercises/exercise12.md)

Further the following exercises do not use Azure and can be completed without setting up any services:

- Exercise 04 XML JSON
- Exercise 06 DataModels
- Exercise 13 OLAP Cubes

&nbsp;

[Open an issue](https://gitlab.ethz.ch/tgeorg/bigdata-local/-/issues) if you encounter any problems following the instructions or find any mistakes.

[Merge requests](https://gitlab.ethz.ch/tgeorg/bigdata-local/-/merge_requests) to expand the instructions are also welcome
